﻿using Microsoft.EntityFrameworkCore;
using WishListApi.Data;
using WishListApi.Models;

namespace WishListApi.Services
	{
	public class WishService
	{
		private readonly ApiContext _context;

		public WishService(ApiContext context)
		{
			_context = context;
		}

		public List<WishList> GetWishLists()
		{
			var lists = _context.WishLists.Include(x => x.Items).ToList();

			if (lists == null)
			{
				return null;
			}

			var result = lists;
			return result;
		}

		public List<WishItem> GetWishItems(Guid listId)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);

			var items = list?.Items.ToList();

			if (items == null)
			{
				return null;
			}

			var result = new List<WishItem>();
			result = items;
			return result;
		}

		public WishList GetWishList(Guid id)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == id);

			if(list == null)
			{
				return null;
			}

			return list;
		}

		public WishItem GetWishItem(Guid listId, Guid itemId)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);

			if (list == null)
			{
				return null;
			}

			var item = list.Items.FirstOrDefault(x => x.Id == itemId);

			if (item == null)
			{
				return null;
			}

			var result = new WishItem();
			result = item;
			return result;
		}

		public bool DeleteWishList(Guid listId)
		{
			var list = _context.WishLists.FirstOrDefault(x => x.Id == listId);

			if (list != null)
			{
				_context.WishLists.Remove(list);			
				_context.SaveChanges();
				return true;
			}
			return false;
		}

		public bool DeleteWishItem(Guid listId, Guid itemId)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);

			if (list != null)
			{
				var item = list.Items.FirstOrDefault(item => item.Id == itemId);
				if (item == null)
				{
					return false;
				}
				_context.WishItems.Remove(item);				
				_context.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}
		public WishList CreateWishList(WishList wishList)
		{
			_context.WishLists.Add(wishList);
			_context.SaveChanges();
			return wishList;

		}

		public WishItem CreateWishItem(Guid listId, WishItem wishItem)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);
			
			if (list == null)
			{
				return null;
			}

			wishItem.WishListId = listId;
			list.Items.Add(wishItem);
			_context.SaveChanges();
			return wishItem;
		}

		public bool UpdateWishList(Guid id, WishList wishList)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == id);

			if (list != null)
			{
				list.Update(wishList);
				_context.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool UpdateWishItem(Guid listId, Guid itemId, WishItem wishItem)
		{
			var list = _context.WishLists.Include(x => x.Items).FirstOrDefault(x => x.Id == listId);

			if (list != null)
			{
				var item = list.Items.FirstOrDefault(item => item.Id == itemId);
				if (item == null)
				{
					return false;
				}
				item.Update(wishItem);
				_context.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		public List<WishList> Search(string title)
		{
			if (String.IsNullOrEmpty(title))
			{
				return GetWishLists();
			}

			var lists = _context.WishLists.Where(x => x.Title.Contains(title)).ToList();

			if (lists == null)
			{
				return null;
			}

			var result = new List<WishList>();
			lists.ForEach(list => result.Add(list));
			return result;
		}

	}
}
