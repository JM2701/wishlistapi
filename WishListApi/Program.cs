using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using WishListApi.Data;
using WishListApi.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApiContext>(
		options => options.UseSqlServer(
			builder.Configuration.GetConnectionString("DefaultConnection"
			))
	);

builder.Services.AddSwaggerGen(c =>
{
	c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
});

builder.Services.AddTransient<WishService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseSwagger();

app.UseSwaggerUI(c =>
{
	c.SwaggerEndpoint("v1/swagger.json", "My API V1");
});

using (var scope = app.Services.CreateScope())
{
	var datacontext = scope.ServiceProvider.GetRequiredService<ApiContext>();
	datacontext.Database.Migrate();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
