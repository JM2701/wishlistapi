﻿using Microsoft.EntityFrameworkCore;
using WishListApi.Models;

namespace WishListApi.Data
{
	public class ApiContext : DbContext
	{
		public ApiContext(DbContextOptions<ApiContext> options) : base(options)
		{

		}

		public DbSet<WishList> WishLists { get; set; }
		public DbSet<WishItem> WishItems { get; set; }
	}
}
