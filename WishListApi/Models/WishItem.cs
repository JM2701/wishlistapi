﻿using System.Runtime.CompilerServices;

namespace WishListApi.Models
{
	public class WishItem
	{
		public Guid Id { get; set; }
		public string Text { get; set; }
		public bool IsFulfilled { get; set; }
		public Guid WishListId { get; set; }

		public void Update(WishItem wishItem)
		{
			Text = wishItem.Text;
			IsFulfilled = wishItem.IsFulfilled;
		}
	}
}