﻿namespace WishListApi.Models
{
	public class WishList
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public List<WishItem> Items { get; set; } = new();
		public string Owner { get; set; }

		public void Update(WishList wishList)
		{
			Title = wishList.Title;
		}
	}
}
