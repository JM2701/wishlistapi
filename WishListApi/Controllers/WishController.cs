﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WishListApi.Models;
using WishListApi.Data;
using WishListApi.Services;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace WishListApi.Controllers
{
	[Route("api/wish-lists")]
	[ApiController]
	public class WishController : ControllerBase
	{
		private readonly WishService _service;

		public WishController(WishService service) =>
				_service = service ?? throw new ArgumentNullException(nameof(service));


		[HttpGet]
		[ProducesResponseType(typeof(List<WishList>), 200)]
		public IActionResult GetWishLists()
		{
			var lists = _service.GetWishLists();
			return lists == null ? NotFound() : Ok(lists);
		}

		[HttpGet("{listId}/wish-items")]
		public IActionResult GetWishItems([FromRoute] Guid listId)
		{
			var items = _service.GetWishItems(listId);
			return items == null ? NotFound() : Ok(items);
		}

		[HttpGet("{id}")]
		[ProducesResponseType(typeof(List<WishList>), (int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.NotFound)]
		public IActionResult GetWishList([FromRoute] Guid id)
		{
			var list = _service.GetWishList(id);
			return list == null ? NotFound() : Ok(list);
		}

		[HttpGet("{listId}/wish-items/{itemId}")]
		public IActionResult GetWishItem([FromRoute] Guid listId, [FromRoute] Guid itemId)
		{
			var item = _service.GetWishItem(listId, itemId);
			return item == null ? NotFound() : Ok(item);
		}

		[HttpPost]
		public IActionResult CreateWishList([FromBody] WishList wishList)
		{
			var result = _service.CreateWishList(wishList);
			return result != null ? CreatedAtAction(nameof(WishList), new { id = wishList.Id }, result) : NotFound();//putanja do resursa i sam resurs
		}

		[HttpPost("{listId}/wish-items")]
		public IActionResult CreateWishItem([FromRoute] Guid listId, [FromBody] WishItem wishItem)
		{
			var result = _service.CreateWishItem(listId, wishItem);
			return result != null ? CreatedAtAction(nameof(GetWishItem), new { listId, itemId = result.Id }, result) : NotFound();
		}

		[HttpPut("{id}")]
		public IActionResult UpdateWishList([FromRoute] Guid id, [FromBody] WishList wishList)
		{
			var list = _service.UpdateWishList(id, wishList);
			return list ? NoContent() : NotFound();
		}

		[HttpPut("{listId}/wish-items/{itemId}")]
		public IActionResult UpdateWishItem([FromRoute] Guid listId, [FromRoute] Guid itemId, [FromBody] WishItem wishItem)
		{
			var item = _service.UpdateWishItem(listId,itemId, wishItem);
			return item ? NoContent() : NotFound();
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteWishList([FromRoute] Guid id)
		{
			var list = _service.DeleteWishList(id);
			return list ? NoContent() : NotFound();
		}

		[HttpDelete("{listId}/wish-items/{itemId}")]
		public IActionResult DeleteWishItem([FromRoute] Guid listId, [FromRoute] Guid itemId)
		{
			var item = _service.DeleteWishItem(listId, itemId);
			return item ? NoContent() : NotFound();
		}

		[HttpGet("search")]
		public IActionResult Search([FromQuery] string? title)
		{
			var ret = _service.Search(title);
			return Ok(ret);
		}
	}
}
